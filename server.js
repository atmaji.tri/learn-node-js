import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import db from './src/app/index.js';
import RoutesInitial from './src/initialization/RoutesInitial.js'

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to test application." });
});

// Drop Table First Run
// db.sequelize.drop();

// sync model to database 
// db.sequelize.sync();

// setup routes
// RoutesInitial(app)

// set port, listen for requests
const PORT = process.env.PORT || 8086;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});