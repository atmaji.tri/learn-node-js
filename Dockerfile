FROM node:16.13.0-alpine

WORKDIR /app

COPY . .

EXPOSE 8086

CMD ["npm", "start"]