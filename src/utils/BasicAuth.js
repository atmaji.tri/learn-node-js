import Base64 from 'js-base64';

const getUserName = (auth) => {
    const header = auth.replace("Basic ", "");
    const parse = Base64.decode(header).split(":");
    const value = parse[0].toString();

    return value
}

const getPassword = (auth) => {
    const header = auth.replace("Basic ", "");
    const parse = Base64.decode(header).split(":");
    const value = parse[1].toString();

    return value
}

export default {
    getUserName,
    getPassword
}