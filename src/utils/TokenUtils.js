import jwt from 'jsonwebtoken';
import { v4 } from 'uuid';

const generateToken = (voCredential) => {
    const id = v4().toString()
    const key = jwt.sign(voCredential, id)
    const result = {
        id: id,
        key: key
    }
    return result
}

const decodeToken = (token) => {
    const decode = jwt.verify(token.key, token.id)
    return decode
}

export default {
    generateToken,
    decodeToken
}