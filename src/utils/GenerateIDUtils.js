const GenerateID = (prefix) => {
    const dateNow = new Date();
    const id = prefix + dateNow.getFullYear().toString() + dateNow.getMonth().toString() + dateNow.getDate().toString() + dateNow.getHours().toString() + dateNow.getMinutes().toString() + dateNow.getSeconds().toString() + dateNow.getMilliseconds().toString()
    return id
}

export default { GenerateID }