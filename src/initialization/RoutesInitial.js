import app000 from '../app/app000/routes/app000Route.js';
import app001 from '../app/app001/routes/app001Route.js';
import app002 from '../app/app002/routes/app002Routes.js';

const RoutesInitial = (app) => {
    // register routes app
    app000(app);
    app001(app);
    app002(app);
}

export default RoutesInitial;