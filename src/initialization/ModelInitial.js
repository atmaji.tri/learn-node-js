const ModelInitial = (Sequelize) => {
    const model = {
        createby: {
            type: Sequelize.STRING(1000)
        },
        createdAt: Sequelize.DATEONLY,
        updateby: {
            type: Sequelize.STRING(1000)
        },
        updatedAt: Sequelize.DATEONLY
    }
    return model
}

export default ModelInitial;