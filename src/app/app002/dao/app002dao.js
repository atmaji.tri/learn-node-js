import db from '../../index.js';

const sessionPasien = db.txnpasien;

const operation = db.Sequelize.Op;

const createDataPasien = (req) => {
    return sessionPasien.create(req)
        .then(val => val)
        .catch(err => err)
}

const updateDataPasien = (req) => {
    return sessionPasien.update(req, { where: { vpid: req.vpid } })
        .then(val => val)
        .catch(err => err)
}

const deleteDataPasien = (req) => {
    return sessionPasien.destroy({ where: { vpid: req.vpid } })
        .then(value => value)
        .catch(err => err)
}

const findAllPasien = () => {
    return sessionPasien.findAll()
        .then(value => value)
        .catch(err => err)
}

const findByNik = (req) => {
    return sessionPasien.findOne({
        where: { vnik: req.vnik }
    })
        .then(val => val)
        .catch(err => err)
}

export default {
    createDataPasien,
    updateDataPasien,
    deleteDataPasien,
    findAllPasien,
    findByNik
}