import { TokenUtils } from '../../../utils/index.js';
import app002Assembler from '../assembler/index.js';
import app002dao from '../dao/index.js';
import { GenerateIDUtils } from '../../../utils/index.js';

const createData = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.birthdate) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const request = app002Assembler.ToDomain(req.body);
    app002dao.findByNik(request)
        .then(val => {
            if (val == null) {
                const userLoginVo = TokenUtils.decodeToken(req.headers)
                app002dao.createDataPasien({ ...request, createby: userLoginVo.vemail, updateby: userLoginVo.vemail, vpid: GenerateIDUtils.GenerateID("PSN") })
                    .then(value => {
                        res.send(value)
                    })
                    .catch(err => {
                        res.status(500).send({
                            message:
                                err.message || "Some error occurred while find the User."
                        });
                    })
            } else {
                res.status(500).send({
                    message: "Data Already Exist"
                });
            }

        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while find the User."
            });
        })

}

const updateData = (req, res) => {
    const request = app002Assembler.ToDomain(req.body)
    const userLoginVo = TokenUtils.decodeToken(req.headers)
    app002dao.updateDataPasien({ ...request, updateby: userLoginVo.vemail })
        .then(value => res.send({
            message: value[0] === 1 ? "SUCCESS" : "FAILED",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const deleteData = (req, res) => {
    app002dao.deleteDataPasien(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const findAllData = (req, res) => {
    app002dao.findAllPasien()
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

export default {
    createData,
    updateData,
    deleteData,
    findAllData
}