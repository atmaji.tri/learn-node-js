import ModelInitial from '../../../initialization/ModelInitial.js';

const txnpasienTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vpid: {
            type: Sequelize.STRING(1000),
            primaryKey: true
        },
        ...init,
        vname: {
            type: Sequelize.STRING(1000),
        },
        vnamemate: {
            type: Sequelize.STRING(1000),
        },
        vaddress: {
            type: Sequelize.STRING(1000),
        },
        dbirthdate: {
            type: Sequelize.DATEONLY
        },
        vage: {
            type: Sequelize.STRING(1000),
        },
        vagemate: {
            type: Sequelize.STRING(1000),
        },
        vgender: {
            type: Sequelize.STRING(1000),
        },
        vmarital: {
            type: Sequelize.STRING(1000),
        },
        vlaststatus: {
            type: Sequelize.STRING(1000),
        },
        vnik:{
            type: Sequelize.STRING(1000),
        },
        vkk:{
            type: Sequelize.STRING(1000),
        }
    }
    const txnpasien = sequelize.define("txn_pasien", model);
    return txnpasien;

}

export default txnpasienTable