const ToDomain = (app002VoPasien) => {
    const app000DomainPasien = {
        vpid: app002VoPasien.vpid || '',
        vname: app002VoPasien.name || '',
        vnamemate: app002VoPasien.namemate || '',
        vaddress: app002VoPasien.address || '',
        dbirthdate: new Date(app002VoPasien.birthdate) || '',
        vage: app002VoPasien.age || '',
        vagemate: app002VoPasien.agemate || '',
        vgender: app002VoPasien.gender || '',
        vmarital: app002VoPasien.marital || '',
        vlaststatus: app002VoPasien.laststatus || '',
        updateby: app002VoPasien.updateby || '',
        vnik: app002VoPasien.nik || '',
        vkk: app002VoPasien.kk || '',
    };

    return app000DomainPasien;
}

const ToDomains = (app002VoPasien) => {
    const app000DomainPasien = app002VoPasien.map(obj => {
        return ToDomain(obj)
    })
    return app000DomainPasien;
}

export default {
    ToDomain,
    ToDomains
}