import express from 'express';
import app002Controller from '../controllers/index.js';

export default app002 => {
    var router = express.Router();

    // Create a new api url pasien
    router.post("/create-pasien", app002Controller.createData);

    router.post("/delete-pasien", app002Controller.deleteData);

    router.post("/search-all-pasien", app002Controller.findAllData);

    router.post("/update-pasien", app002Controller.updateData);

    app002.use("/api/pasien", router);
}