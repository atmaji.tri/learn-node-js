import ModelInitial from '../../../initialization/ModelInitial.js';

const mstroleTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vroleid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ...init,
        dbgneff: {
            type: Sequelize.DATEONLY
        },
        dendeff: {
            type: Sequelize.DATEONLY
        }, 
        vrole: {
            type: Sequelize.STRING(1000)
        }, 
        vdesc: {
            type: Sequelize.STRING(1000)
        },

    }

    const mstrole = sequelize.define("uam_mstrole", model);
    return mstrole;
}

export default mstroleTable;