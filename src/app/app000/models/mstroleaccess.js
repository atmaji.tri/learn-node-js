import ModelInitial from '../../../initialization/ModelInitial.js';

const mstroleaccessTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vrlaccid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ...init,
        dbgneff: {
            type: Sequelize.DATEONLY
        },
        dendeff: {
            type: Sequelize.DATEONLY
        },
        iseq: {
            type: Sequelize.INTEGER
        }
    }

    const mstroleaccess = sequelize.define("uam_mstroleaccess", model);
    return mstroleaccess;
}

export default mstroleaccessTable;