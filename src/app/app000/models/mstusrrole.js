import ModelInitial from '../../../initialization/ModelInitial.js';

const mstusrroleTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ...init,
        dbgneff: {
            type: Sequelize.DATEONLY
        },
        dendeff: {
            type: Sequelize.DATEONLY
        },
        // vid_uam_mstrole: {
        //     type: Sequelize.INTEGER
        // },
        // vid_uam_mstusers: {
        //     type: Sequelize.INTEGER
        // }
    }

    const mstusrrole = sequelize.define("uam_mstusrrole", model);
    return mstusrrole;
}

export default mstusrroleTable;