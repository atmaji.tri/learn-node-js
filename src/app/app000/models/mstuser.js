import ModelInitial from '../../../initialization/ModelInitial.js';

const mstuserTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vuid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ...init,
        vusername: {
            type: Sequelize.STRING(1000)
        },
        vpassword: {
            type: Sequelize.STRING(1000)
        },
        vemail: {
            type: Sequelize.STRING(1000)
        },
        vnpk: {
            type: Sequelize.STRING(10)
        },
        vfullname: {
            type: Sequelize.STRING(1000)
        },
        bstatus: {
            type: Sequelize.BOOLEAN
        }
    }
    const mstuser = sequelize.define("uam_mstuser", model);
    return mstuser;
}

export default mstuserTable;