import app000dao from '../dao/index.js';
import app000Assembler from '../assembler/index.js';
import { BasicAuth, TokenUtils } from '../../../utils/index.js';
import crypto from 'crypto-js'


const register = (req, res) => {
    // Validate request
    if (!req.body.username) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const request = app000Assembler.ToDomain(req.body);
    const pass = crypto.SHA256(request.vpassword).toString()
    app000dao.createUser({ ...request, vpassword: pass }, res);
}

const login = (req, res) => {
    const username = BasicAuth.getUserName(req.headers.authorization);
    const password = BasicAuth.getPassword(req.headers.authorization);
    const pass = crypto.SHA256(password).toString()
    app000dao.findByUserAndPass({ vusername: username, vpassword: pass })
        .then(val => {
            const token = TokenUtils.generateToken(val.dataValues)
            app000dao.findByIdUserRole({ vid_uam_mstusers: val.vuid })
                .then(usrRole => {
                    res.send({ data: val, token: token, dataRole: { idusrrole: usrRole.vid, role: usrRole.uam_mstrole.vdesc } })
                })
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while find the User."
            });
        })
}

const deleteUser = (req, res) => {
    app000dao.deleteData(req.body)
        .then(value => {
            res.send(value)
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while find the User."
            });
        })
}

const findAllUsers = (req, res) => {
    app000dao.findAllUser()
        .then(value => {
            res.send(value)
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while find the User."
            });
        })
}

const updateData = (req, res) => {
    app000dao.update(req.body)
        .then(value => res.send({
            message: "SUCCESS",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}
// Controller Role Management
const createRole = (req, res) => {
    if (!req.body.vrole) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    app000dao.createRole(req.body)
        .then(value =>
            res.send({
                data: value[0],
                status: value[1],
                message: value[1] === true ? "SUCCESS" : "FAILED"
            }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const updateRole = (req, res) => {
    app000dao.updateRole(req.body)
        .then(value => res.send({
            message: "SUCCESS",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const findByIdRole = (req, res) => {
    app000dao.findByIdRole(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const deleteDataRole = (req, res) => {
    app000dao.deleteDataRole(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const findAllDataRole = (req, res) => {
    app000dao.findAllRole()
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

// Controller Role Access Management
const createRoleAccess = (req, res) => {
    app000dao.createRoleAccess(req.body)
        .then(value =>
            res.send({
                data: value[0],
                status: value[1],
                message: value[1] === true ? "SUCCESS" : "FAILED"
            }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const updateRoleAccess = (req, res) => {
    app000dao.updateRoleAccess(req.body)
        .then(value => res.send({
            message: "SUCCESS",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const findByIdRoleAccess = (req, res) => {
    app000dao.findByIdRoleAccess(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const deleteDataRoleAccess = (req, res) => {
    app000dao.deleteDataRoleAccess(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const findAllDataRoleAccess = (req, res) => {
    app000dao.findAllRoleAccess(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

// Controller User Role Management
const createUserRole = (req, res) => {
    app000dao.createUserRole(req.body)
        .then(value =>
            res.send({
                data: value[0],
                status: value[1],
                message: value[1] === true ? "SUCCESS" : "FAILED"
            }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const updateUserRole = (req, res) => {
    app000dao.updateUserRole(req.body)
        .then(value => res.send({
            message: "SUCCESS",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const findByIdUserRole = (req, res) => {
    app000dao.findByIdUserRole(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const deleteDataUserRole = (req, res) => {
    app000dao.deleteDataUserRole(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const findAllDataUserRole = (req, res) => {
    app000dao.findAllUserRole()
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

export default {
    register,
    login,
    deleteUser,
    findAllUsers,
    updateData,

    createRole,
    updateRole,
    findByIdRole,
    deleteDataRole,
    findAllDataRole,

    createRoleAccess,
    updateRoleAccess,
    findByIdRoleAccess,
    deleteDataRoleAccess,
    findAllDataRoleAccess,

    createUserRole,
    updateUserRole,
    findByIdUserRole,
    deleteDataUserRole,
    findAllDataUserRole,
}
