import db from '../../index.js';

const sessionUser = db.mstuser
const sessionRole = db.mstrole
const sessionRoleAccess = db.mstroleaccess
const sessionUserRole = db.mstusrrole

const operation = db.Sequelize.Op;

// Dao User management
const createUser = (req, res) => {
    sessionUser.findOrCreate({ where: { vusername: req.vusername }, defaults: req })
        .then(val => {
            res.send({
                data: val[0],
                status: val[1],
                message: val[1] === true ? "SUCCESS" : "FAILED"
            })
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        })
}

const findByUserAndPass = (req) => {
    return sessionUser.findOne({ where: { vusername: req.vusername, vpassword: req.vpassword } })
        .then(val => val)
        .catch(err => err)
}

const update = (req) => {
    return sessionUser.update(req, { where: { vusername: req.vusername } })
        .then(val => val)
        .catch(err => err)
}

const deleteData = (req) => {
    return sessionUser.destroy({ where: { vuid: req.vuid } })
        .then(value => value)
        .catch(err => err)
}

const findAllUser = () => {
    return sessionUser.findAll()
        .then(value => value)
        .catch(err => err)
}

// Dao Role Management
const createRole = (req) => {
    return sessionRole.findOrCreate({ where: { vrole: req.vrole }, defaults: req })
        .then(val => val)
        .catch(err => err)
}

const updateRole = (req => {
    return sessionRole.update(req, { where: { vroleid: req.vroleid } })
        .then(val => val)
        .catch(err => err)
})

const findByIdRole = (req) => {
    return sessionRole.findOne({ where: { vroleid: req.vroleid } })
        .then(val => val)
        .catch(err => err)
}

const deleteDataRole = (req) => {
    return sessionRole.destroy({ where: { vroleid: req.vroleid } })
        .then(value => value)
        .catch(err => err)
}

const findAllRole = () => {
    return sessionRole.findAll()
        .then(value => value)
        .catch(err => err)
}

// Dao Role Access Management
const createRoleAccess = (req) => {
    return sessionRoleAccess.findOrCreate({ where: { vrlaccid: req.vrlaccid }, defaults: req })
        .then(val => val)
        .catch(err => err)
}

const updateRoleAccess = (req => {
    return sessionRoleAccess.update(req, { where: { vrlaccid: req.vrlaccid } })
        .then(val => val)
        .catch(err => err)
})

const findByIdRoleAccess = (req) => {
    return sessionRoleAccess.findOne({
        where: { vrlaccid: req.vrlaccid }, include: [{
            model: sessionUserRole.sequelize.models.uam_mstmenu,
            as: "uam_mstmenu"
        }, {
            model: sessionUserRole.sequelize.models.uam_mstusrrole,
            as: "uam_mstusrrole"
        }]
    })
        .then(val => val)
        .catch(err => err)
}

const deleteDataRoleAccess = (req) => {
    return sessionRoleAccess.destroy({ where: { vrlaccid: req.vrlaccid } })
        .then(value => value)
        .catch(err => err)
}

const findAllRoleAccess = (req) => {
    return sessionRoleAccess.findAll({
        where: req.vrlaccid || req.vid_uam_mstusrrole || req.vid_uam_mstmenu ? {
            [operation.or]: [
                { vrlaccid: req.vrlaccid || null },
                { vid_uam_mstusrrole: req.vid_uam_mstusrrole || null },
                { vid_uam_mstmenu: req.vid_uam_mstmenu || null }
            ]
        } : null,
        include: [{
            model: sessionUserRole.sequelize.models.uam_mstmenu,
            as: "uam_mstmenu"
        }, {
            model: sessionUserRole.sequelize.models.uam_mstusrrole,
            as: "uam_mstusrrole"
        }]
    })
        .then(value => value)
        .catch(err => err)
}

// Dao User Role Management
const createUserRole = (req) => {
    return sessionUserRole.findOrCreate({ where: { vid_uam_mstusers: req.vid_uam_mstusers }, defaults: req })
        .then(val => val)
        .catch(err => err)
}

const updateUserRole = (req => {
    return sessionUserRole.update(req, { where: { vid: req.vid } })
        .then(val => val)
        .catch(err => err)
})

const findByIdUserRole = (req) => {
    return sessionUserRole.findOne({
        where: req.vid || req.vid_uam_mstusers || req.vid_uam_mstrole ? {
            [operation.or]: [
                { vid: req.vid || null },
                { vid_uam_mstusers: req.vid_uam_mstusers || null },
                { vid_uam_mstrole: req.vid_uam_mstrole || null }
            ]
        } : null,
        include: [
            {
                model: sessionUserRole.sequelize.models.uam_mstrole,
                as: "uam_mstrole"
            },
            {
                model: sessionUserRole.sequelize.models.uam_mstuser,
                as: "uam_mstuser"
            }]
    })
        .then(val => val)
        .catch(err => err)
}

const deleteDataUserRole = (req) => {
    return sessionUserRole.destroy({ where: { vid: req.vid } })
        .then(value => value)
        .catch(err => err)
}

const findAllUserRole = () => {
    return sessionUserRole.findAll({
        include: [
            {
                model: sessionUserRole.sequelize.models.uam_mstrole,
                as: "uam_mstrole"
            },
            {
                model: sessionUserRole.sequelize.models.uam_mstuser,
                as: "uam_mstuser"
            }]
    })
        .then(value => value)
        .catch(err => err)
}

export default {
    createUser,
    findByUserAndPass,
    update,
    deleteData,
    findAllUser,

    createRole,
    updateRole,
    findByIdRole,
    deleteDataRole,
    findAllRole,

    createRoleAccess,
    updateRoleAccess,
    findByIdRoleAccess,
    deleteDataRoleAccess,
    findAllRoleAccess,

    createUserRole,
    updateUserRole,
    findByIdUserRole,
    deleteDataUserRole,
    findAllUserRole

}