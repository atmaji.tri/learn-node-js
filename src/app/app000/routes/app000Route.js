import express from 'express';
import app000Controller from '../controllers/index.js'

export default app000 => {
    var router = express.Router();

    // Create a new api url User
    router.post("/register", app000Controller.register);

    router.post("/login", app000Controller.login);

    router.post("/search-all", app000Controller.findAllUsers);

    router.post("/delete-user", app000Controller.deleteUser);

    // Create a new api url Role Management
    router.post("/create-role", app000Controller.createRole);

    router.post("/update-role", app000Controller.updateRole);

    router.post("/search-id-role", app000Controller.findByIdRole);

    router.post("/search-all-role", app000Controller.findAllDataRole);

    router.post("/delete-id-role", app000Controller.deleteDataRole);

    // Create a new api url Role Access Management
    router.post("/create-role-access", app000Controller.createRoleAccess);

    router.post("/update-role-access", app000Controller.updateRoleAccess);

    router.post("/search-id-role-access", app000Controller.findByIdRoleAccess);

    router.post("/search-all-role-access", app000Controller.findAllDataRoleAccess);

    router.post("/delete-id-role-access", app000Controller.deleteDataRoleAccess);

    // Create a new api url User Role Management
    router.post("/create-user-role", app000Controller.createUserRole);

    router.post("/update-user-role", app000Controller.updateUserRole);

    router.post("/search-id-user-role", app000Controller.findByIdUserRole);

    router.post("/search-all-user-role", app000Controller.findAllDataUserRole);

    router.post("/delete-id-user-role", app000Controller.deleteDataUserRole);

    app000.use("/api/user-management", router);
}