const ToDomain = (app000VoUser) => {
    return {
        vusername: app000VoUser.username || '',
        vpassword: app000VoUser.password || '',
        vemail: app000VoUser.email || '',
        vfullname: app000VoUser.fullname || '',
        vnpk: app000VoUser.npk || '',
        bstatus: app000VoUser.status || true,
        createby: app000VoUser.email || '',
        updateby: app000VoUser.email || '',
    };
}

const ToDomains = (app000VoUsers) => {
    return app000VoUsers.map(obj => {
        return ToDomain(obj)
    })
}

export default {
    ToDomain,
    ToDomains
}