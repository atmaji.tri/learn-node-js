import db from '../../index.js';

const sessionMenu = db.mstmenu

const create = (req) => {
    return sessionMenu.findOrCreate({ where: { vnamemenu: req.vnamemenu }, defaults: req })
        .then(val => val)
        .catch(err => err)
}

const update = (req => {
    return sessionMenu.update(req, { where: { vnamemenu: req.vnamemenu } })
        .then(val => val)
        .catch(err => err)
})

const findById = (req) => {
    return sessionMenu.findOne({ where: { vmenuid: req.vmenuid } })
        .then(val => val)
        .catch(err => err)
}

const deleteData = (req) => {
    return sessionMenu.destroy({ where: { vmenuid: req.vmenuid } })
        .then(value => value)
        .catch(err => err)
}

const findAllMenu = () => {
    return sessionMenu.findAll()
        .then(value => value)
        .catch(err => err)
}

export default {
    create,
    update,
    findById,
    deleteData,
    findAllMenu
}