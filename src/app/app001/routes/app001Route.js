import express from 'express';
import app001Controller from '../controllers/index.js'

export default app001 => {
    var router = express.Router();

    // Create a new api url
    router.post("/create", app001Controller.createData);

    router.post("/update", app001Controller.updateData);

    router.post("/search-id", app001Controller.findById);

    router.post("/search-all", app001Controller.findAllData);

    router.post("/delete-id", app001Controller.deleteData);

    router.post("/get-menu", app001Controller.getMenu);

    app001.use('/api/menu', router);
}