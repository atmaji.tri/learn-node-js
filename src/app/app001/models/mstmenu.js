import ModelInitial from '../../../initialization/ModelInitial.js';

const mstmenuTable = (sequelize, Sequelize) => {
    const init = ModelInitial(Sequelize);
    const model = {
        vmenuid: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ...init,
        dbgneff: {
            type: Sequelize.DATEONLY
        },
        dendeff: {
            type: Sequelize.DATEONLY
        },
        iseq: {
            type: Sequelize.INTEGER
        },
        bisparent: {
            type: Sequelize.BOOLEAN
        }, 
        vparent: {
            type: Sequelize.STRING(1000)
        },
        vurl: {
            type: Sequelize.STRING(1000)
        },
        vicon: {
            type: Sequelize.STRING(1000)
        },
        vnamemenu: {
            type: Sequelize.STRING(1000)
        },
        vstatus: {
            type: Sequelize.STRING(1000)
        },

    }

    const mstmenu = sequelize.define("uam_mstmenu", model);
    return mstmenu;
}

export default mstmenuTable;