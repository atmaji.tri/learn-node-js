const ToDomain = (app001VoMenu) => {
    const app001DomainMenu = {
        // dbgneff: app001VoMenu.dstart || '',
        // dendeff: app001VoMenu.dend || '',
        iseq: app001VoMenu.seq || '',
        bisparent: app001VoMenu.isparent || false,
        vparent: app001VoMenu.parent || '',
        vurl: app001VoMenu.url || '',
        vicon: app001VoMenu.icon || '',
        vnamemenu: app001VoMenu.namemenu || '',
        vstatus: app001VoMenu.status || '',
    };

    return app001DomainMenu;
}

const ToDomains = (app001VoMenus) => {
    const app001DomainsMenu = app001VoMenus.map(obj => {
        return ToDomain(obj)
    })
    return app001DomainsMenu;
}

export default {
    ToDomain,
    ToDomains
}