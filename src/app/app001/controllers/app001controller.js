import app001dao from '../dao/index.js';
import app001Assembler from '../assembler/index.js';
import app000dao from '../../app000/dao/app000dao.js';

const createData = (req, res) => {
    if (!req.body.namemenu) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const request = app001Assembler.ToDomain(req.body)
    app001dao.create(request)
        .then(value =>
            res.send({
                data: value[0],
                status: value[1],
                message: value[1] === true ? "SUCCESS" : "FAILED"
            }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const updateData = (req, res) => {
    const request = app001Assembler.ToDomain(req.body)
    app001dao.update(request)
        .then(value => res.send({
            message: "SUCCESS",
            status: value[0] === 1 ? true : false
        }))
        .catch(err =>
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            })
        )
}

const findById = (req, res) => {
    app001dao.findById(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const deleteData = (req, res) => {
    app001dao.deleteData(req.body)
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const findAllData = (req, res) => {
    app001dao.findAllMenu()
        .then(value => res.send(value))
        .catch(err => res.status(500).send({
            message:
                err.message || "Some error occurred while creating the User."
        }))
}

const getMenu = (req, res) => {
    app000dao.findAllRoleAccess(req.body)
        .then(value => {
            res.send(value)
        })
}

export default {
    createData,
    updateData,
    findById,
    deleteData,
    findAllData,
    getMenu
}
