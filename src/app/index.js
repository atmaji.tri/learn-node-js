import dbConfig from '../config/db.config.js';
import Sequelize from 'sequelize';
import mstuserTable from './app000/models/mstuser.js';
import mstusrroleTable from './app000/models/mstusrrole.js';
import mstroleTable from './app000/models/mstrole.js';
import mstmenuTable from './app001/models/mstmenu.js';
import mstroleaccessTable from './app000/models/mstroleaccess.js';
import txnpasienTable from './app002/models/txnpasien.js';

// create connection to db
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  // Comment if use local uncomment if use heroku db
  // dialectOptions: {
  //   ssl: {
  //     rejectUnauthorized: false, // very important
  //   }
  // },

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

/* =============================== define table ================================ */
// base franework define table
db.mstuser = mstuserTable(sequelize, Sequelize);
db.mstusrrole = mstusrroleTable(sequelize, Sequelize);
db.mstrole = mstroleTable(sequelize, Sequelize);
db.mstmenu = mstmenuTable(sequelize, Sequelize);
db.mstroleaccess = mstroleaccessTable(sequelize, Sequelize);

// transaction define table
db.txnpasien = txnpasienTable(sequelize, Sequelize);

/* ============================================================================== */

/* =========================== define relation table ============================ */
db.mstuser.hasOne(db.mstusrrole, { as: "uam_mstusrrole" });
db.mstusrrole.belongsTo(db.mstuser, {
  foreignKey: "vid_uam_mstusers",
  as: "uam_mstuser"
});

db.mstrole.hasOne(db.mstusrrole, { as: "uam_mstusrrole" });
db.mstusrrole.belongsTo(db.mstrole, {
  foreignKey: "vid_uam_mstrole",
  as: "uam_mstrole"
});

db.mstusrrole.hasMany(db.mstroleaccess, { as: "uam_mstroleaccess" });
db.mstroleaccess.belongsTo(db.mstusrrole, {
  foreignKey: "vid_uam_mstusrrole",
  as: "uam_mstusrrole"
});

db.mstmenu.hasMany(db.mstroleaccess, { as: "uam_mstroleaccess" });
db.mstroleaccess.belongsTo(db.mstmenu, {
  foreignKey: "vid_uam_mstmenu",
  as: "uam_mstmenu"
})

/* =================================================================================== */
export default db